# Clock Generator

A clock generator with a Si5351 a STM32F103C8 (blue pill) and two outputs. Frequency range from 1 mHz to 40 MHz.