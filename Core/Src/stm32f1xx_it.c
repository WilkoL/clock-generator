
#include "main.h"
#include "stm32f1xx_it.h"

//volatile uint16_t timeout;

volatile int8_t rotary_1;
volatile uint8_t button_1;

volatile int8_t rotary_2;
volatile uint8_t button_2;


void NMI_Handler(void)
{

}


void HardFault_Handler(void)
{
	while (1)
	{

	}
}


void MemManage_Handler(void)
{
	while (1)
	{

	}
}


void BusFault_Handler(void)
{
	while (1)
	{

	}
}


void UsageFault_Handler(void)
{
	while (1)
	{

	}
}


void SVC_Handler(void)
{

}


void DebugMon_Handler(void)
{

}


void PendSV_Handler(void)
{

}


void SysTick_Handler(void)
{
	//if (timeout) timeout--;
	//if (timeout == 1)
	//{
	//
	//}
}



void TIM4_IRQHandler(void)											//10 kHz (debouncing)
{
	static uint8_t rotary_1_pinA;
	static uint8_t rotary_1_pinB;
	static uint16_t rotary_1_button;

	static uint8_t rotary_2_pinA;
	static uint8_t rotary_2_pinB;
	static uint16_t rotary_2_button;


	if (LL_TIM_IsActiveFlag_UPDATE(TIM4))
	{
		LL_TIM_ClearFlag_UPDATE(TIM4);

		rotary_1_pinA <<= 1;
		rotary_1_pinB <<= 1;
		rotary_1_button <<= 1;

		rotary_2_pinA <<= 1;
		rotary_2_pinB <<= 1;
		rotary_2_button <<= 1;

		if (!LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_8)) rotary_1_pinA++;
		if (!LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_9)) rotary_1_pinB++;
		if (!LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_11)) rotary_1_button++;

		if (!LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_4)) rotary_2_pinA++;
		if (!LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_5)) rotary_2_pinB++;
		if (!LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_12)) rotary_2_button++;

		if (rotary_1_pinA == 0x80)
		{
			if (rotary_1_pinB == 0x00) rotary_1 = -1;
			if (rotary_1_pinB == 0xFF) rotary_1 = 1;
		}

		if (rotary_2_pinA == 0x80)
		{
			if (rotary_2_pinB == 0x00) rotary_2 = -1;
			if (rotary_2_pinB == 0xFF) rotary_2 = 1;
		}


		if (rotary_1_button == 0x7FFF) button_1 = 1;
		if (rotary_2_button == 0x7FFF) button_2 = 1;
	}
}
