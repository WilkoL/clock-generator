
#include "main.h"

#define CHANNEL1				0
#define CHANNEL2				1

#define ULTIMATE_SI5351_FREQ	4000000UL		//will be multiplied by 10
#define MAX_SI5351_FREQ			10000000UL
#define MIN_SI5351_FREQ 		1000000UL

#define MAX_FREQ_RANGE_CH1		10
#define MAX_FREQ_RANGE_CH2		8
#define MIN_FREQ_RANGE			0

#define MAX_DIGIT				7
#define MIN_DIGIT				0


typedef struct
{
	uint32_t frequency;
	uint32_t max_frequency;
	int8_t   freq_range;
	int8_t   max_freq_range;
	int8_t   active_digit;
} channel_settings;


void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_IWDG_Init(void);

static void select_frequency_range(TIM_TypeDef *TIMx, channel_settings ch);
static channel_settings calculate_si5351_frequency(channel_settings ch, int8_t up_down);
static void display_frequency(uint8_t channel, channel_settings ch);
static void display_cursor(uint8_t channel, channel_settings ch);


extern volatile int8_t rotary_1;
extern volatile uint8_t button_1;

extern volatile int8_t rotary_2;
extern volatile uint8_t button_2;

channel_settings ch1;
channel_settings ch2;


int main(void)
{
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	LL_GPIO_AF_Remap_SWJ_NOJTAG();

	SystemClock_Config();
	MX_GPIO_Init();
	MX_I2C1_Init();
	MX_SPI2_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_TIM4_Init();
	MX_IWDG_Init();

	LL_mDelay(1);

	LL_I2C_Enable(I2C1);
	si5351_init(SI5351_CRYSTAL_LOAD_8PF, 0);
	si5351_set_correction(90);

	LL_SPI_Enable(SPI2);
	ST7735_Init();
	ST7735_FillScreen(ST7735_DARK_BLUE);
	ST7735_WriteString(30, 30, "Clock", Font_11x18, ST7735_WHITE, ST7735_DARK_BLUE);
	ST7735_WriteString(30, 50, "Generator", Font_11x18, ST7735_WHITE, ST7735_DARK_BLUE);
	LL_mDelay(2000);
	ST7735_FillScreen(ST7735_DARK_BLUE);

	ST7735_WriteString(15, 10, "Channel 1", Font_11x18, ST7735_LIGHT_GRAY , ST7735_DARK_BLUE);
	ST7735_WriteString(15, 70, "Channel 2", Font_11x18, ST7735_LIGHT_GRAY, ST7735_DARK_BLUE);

	LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH2);			//divider of freq1
	LL_TIM_EnableCounter(TIM2);

	LL_TIM_CC_EnableChannel(TIM3, LL_TIM_CHANNEL_CH2);			//divider of freq2
	LL_TIM_EnableCounter(TIM3);

	LL_TIM_EnableIT_UPDATE(TIM4);								//10kHz debouncing
	LL_TIM_EnableCounter(TIM4);

	//LL_SYSTICK_EnableIT();									//1 ms timeout




	ch1.frequency = MIN_SI5351_FREQ;
	ch1.max_frequency = ULTIMATE_SI5351_FREQ;
	ch1.active_digit = MAX_DIGIT;
	ch1.freq_range = 6;
	ch1.max_freq_range = MAX_FREQ_RANGE_CH1;

	ch2.frequency = MIN_SI5351_FREQ;
	ch2.max_frequency = MAX_SI5351_FREQ;
	ch2.active_digit = MAX_DIGIT;
	ch2.freq_range = 6;
	ch2.max_freq_range = MAX_FREQ_RANGE_CH2;

	si5351_set_freq(ch1.frequency, SI5351_CLK0);
	si5351_set_freq(ch2.frequency, SI5351_CLK1);

	si5351_drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);
	si5351_drive_strength(SI5351_CLK1, SI5351_DRIVE_2MA);

	select_frequency_range(TIM2, ch1);
	select_frequency_range(TIM3, ch2);

	display_frequency(CHANNEL1, ch1);
	display_frequency(CHANNEL2, ch2);

	display_cursor(CHANNEL1, ch1);
	display_cursor(CHANNEL2, ch2);

	LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_2);
	LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_3);

	while (1)
	{
		if ((button_1) || (rotary_1))
		{
			if (button_1)
			{
				if (ch1.active_digit > MIN_DIGIT) ch1.active_digit--;	//0 means set frequency range
				else ch1.active_digit = MAX_DIGIT;		//1,2,3,4,5,6,7 means set digit in freq
				button_1 = 0;
			}

			if (rotary_1)						//either -1 or 1
			{
				if (ch1.active_digit)			//active for digits 1,2,3,4,5,6,7
				{
					ch1 = calculate_si5351_frequency(ch1, rotary_1);
				}
				else							//active only for digit 0 (freq_range)
				{
					ch1.freq_range += rotary_1;
					if (ch1.freq_range < MIN_FREQ_RANGE) ch1.freq_range = MIN_FREQ_RANGE;
					if (ch1.freq_range > MAX_FREQ_RANGE_CH1) ch1.freq_range = MAX_FREQ_RANGE_CH1;

					if ((ch1.freq_range == MAX_FREQ_RANGE_CH1) && (ch1.frequency > ULTIMATE_SI5351_FREQ))
						ch1.frequency = ULTIMATE_SI5351_FREQ;
				}
				rotary_1 = 0;
			}

			if (ch1.freq_range == 10) si5351_set_freq((ch1.frequency * 10), SI5351_CLK0);
			else si5351_set_freq(ch1.frequency, SI5351_CLK0);

			select_frequency_range(TIM2, ch1);
			display_frequency(CHANNEL1, ch1);
			display_cursor(CHANNEL1, ch1);

		}



		if ((button_2) || (rotary_2))
		{
			if (button_2)
			{
				if (ch2.active_digit > MIN_DIGIT) ch2.active_digit-- ;	//0 means set frequency range
				else ch2.active_digit = MAX_DIGIT;		//1,2,3,4,5,6,7 means set digit in freq
				button_2 = 0;
			}
			if (rotary_2)						//either -1 or 1
			{
				if (ch2.active_digit)			//active for digits 1,2,3,4,5,6,7
				{
					ch2 = calculate_si5351_frequency(ch2, rotary_2);
				}
				else							//active only for digit 0 (freq_range)
				{
					ch2.freq_range += rotary_2;
				}

				if (ch2.freq_range < MIN_FREQ_RANGE) ch2.freq_range = MIN_FREQ_RANGE;
				if (ch2.freq_range > MAX_FREQ_RANGE_CH2) ch2.freq_range = MAX_FREQ_RANGE_CH2;
				rotary_2 = 0;
			}

			si5351_set_freq(ch2.frequency, SI5351_CLK1);

			select_frequency_range(TIM3, ch2);
			display_frequency(CHANNEL2, ch2);
			display_cursor(CHANNEL2, ch2);

		}

		//take signal from Si5351 for the 1 to 10 and the 10 to 40 MHz ranges
		if ((ch1.freq_range == MAX_FREQ_RANGE_CH1)	|| 	(ch1.freq_range == (MAX_FREQ_RANGE_CH1 - 1)))
		{
			LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_2);
			LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_3);
		}
		else
		{
			LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_2);
			LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_3);
		}
	}
}


channel_settings calculate_si5351_frequency(channel_settings ch, int8_t up_down)
{
	if (up_down == 1)							//increase the frequency
	{
		switch (ch.active_digit)
		{
		case 1:									//lowest digit, so increase with 1
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 1;
			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;				//if the frequency now is above the max value
				ch.freq_range++;				//divide by 10 and increase the range
				//								//and move the active digit one place lower
				//								//but not to 0 (0 means set frequency_range)
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		case 2:									//second digit so increase with 10
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 10;
			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;
				ch.freq_range++;
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		case 3:									//see above
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 100;
			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;
				ch.freq_range++;
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		case 4:
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 1000;
			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;
				ch.freq_range++;
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		case 5:
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 10000;
			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;
				ch.freq_range++;
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		case 6:
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 100000;
			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;
				ch.freq_range++;
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		case 7:
			if (ch.frequency < MAX_SI5351_FREQ) ch.frequency += 1000000;

			if ((ch.frequency >= MAX_SI5351_FREQ) && (ch.freq_range < ch.max_freq_range))
			{
				ch.frequency /= 10;
				ch.freq_range++;
				if (ch.active_digit > (MIN_DIGIT + 1)) ch.active_digit--;
			}
			break;

		default:
			break;
		}

		//for ch1 the max is 40 MHz, more isn't possible with a 74HC00
		//for ch2 the max is 1 MHz
		if ((ch.frequency >= ch.max_frequency) && (ch.freq_range == ch.max_freq_range))
		{
			ch.frequency = ch.max_frequency;
		}
	}



	if (up_down == -1)							//decrease the frequency
	{
		switch (ch.active_digit)
		{
		case 1:
			if (ch.frequency >= MIN_SI5351_FREQ)
			{
				ch.frequency -= 1;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}
			break;

		case 2:
			if (ch.frequency >= MIN_SI5351_FREQ)
			{
				ch.frequency -= 10;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}
			break;

		case 3:
			if (ch.frequency >= MIN_SI5351_FREQ)
			{
				ch.frequency -= 100;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}

			break;

		case 4:
			if (ch.frequency >= MIN_SI5351_FREQ)
			{
				ch.frequency -= 1000;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}
			break;

		case 5:
			if (ch.frequency >= MIN_SI5351_FREQ)
			{
				ch.frequency -= 10000;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}
			break;

		case 6:

			if (ch.frequency >= MIN_SI5351_FREQ)
			{
				ch.frequency -= 100000;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}
			break;

		case 7:
			if (ch.frequency > MIN_SI5351_FREQ)			//not if_equal_to !!!
			{
				ch.frequency -= 1000000;
				while ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range > MIN_FREQ_RANGE))
				{
					ch.frequency *= 10;
					ch.freq_range--;
					if (ch.active_digit < MAX_DIGIT) ch.active_digit++;
				}
			}
			break;

		default:
			break;
		}


		if ((ch.frequency < MIN_SI5351_FREQ) && (ch.freq_range == MIN_FREQ_RANGE))
		{
			ch.frequency = MIN_SI5351_FREQ;
		}
	}
	return ch;
}


void select_frequency_range(TIM_TypeDef *TIMx, channel_settings ch)
{
	switch (ch.freq_range)
	{
	case 0:										//1 mHz - 9 mHz
		LL_TIM_SetCounter(TIMx, 19990);			//set counter to almost the highest value
		LL_TIM_SetPrescaler(TIMx, 49999);		//so we down't have to wait very long for
		LL_TIM_SetAutoReload(TIMx, 19999);		//the update to occur
		LL_TIM_OC_SetCompareCH2(TIMx, 9999);
		break;

	case 1:										//10 mHz - 99 mHz
		LL_TIM_SetCounter(TIMx, 9990);			//set counter to almost the highest value
		LL_TIM_SetPrescaler(TIMx, 9999);
		LL_TIM_SetAutoReload(TIMx, 9999);
		LL_TIM_OC_SetCompareCH2(TIMx, 4999);
		break;

	case 2:										//100 mHz - 999 mHz
		LL_TIM_SetCounter(TIMx, 9990);			//set counter to almost the highest value
		LL_TIM_SetPrescaler(TIMx, 999);
		LL_TIM_SetAutoReload(TIMx, 9999);
		LL_TIM_OC_SetCompareCH2(TIMx, 4999);
		break;

	case 3:										//1 Hz - 9 Hz
		//LL_TIM_SetCounter(TIMx, 9990);		//not needed as the max wait time for
		LL_TIM_SetPrescaler(TIMx, 99);			//an update is 1 second
		LL_TIM_SetAutoReload(TIMx, 9999);
		LL_TIM_OC_SetCompareCH2(TIMx, 4999);
		break;

	case 4:										//10 Hz - 90 Hz
		//LL_TIM_SetCounter(TIMx, 990);			//see above
		LL_TIM_SetPrescaler(TIMx, 9);
		LL_TIM_SetAutoReload(TIMx, 9999);
		LL_TIM_OC_SetCompareCH2(TIMx, 4999);
		break;

	case 5:										//100 Hz - 900 Hz
		//LL_TIM_SetCounter(TIMx, 990);
		LL_TIM_SetPrescaler(TIMx, 0);
		LL_TIM_SetAutoReload(TIMx, 9999);
		LL_TIM_OC_SetCompareCH2(TIMx, 4999);
		break;

	case 6:										//1 kHz	- 9 kHz
		//LL_TIM_SetCounter(TIMx, 90);
		LL_TIM_SetPrescaler(TIMx, 0);
		LL_TIM_SetAutoReload(TIMx, 999);
		LL_TIM_OC_SetCompareCH2(TIMx, 499);
		break;

	case 7:										//10 kHz - 99 kHz
		//LL_TIM_SetCounter(TIMx, 0);
		LL_TIM_SetPrescaler(TIMx, 0);
		LL_TIM_SetAutoReload(TIMx, 99);
		LL_TIM_OC_SetCompareCH2(TIMx, 49);
		break;


	case 8:										//100 kHz - 999 kHz
		//LL_TIM_SetCounter(TIMx, 0);
		LL_TIM_SetPrescaler(TIMx, 0);
		LL_TIM_SetAutoReload(TIMx, 9);
		LL_TIM_OC_SetCompareCH2(TIMx, 4);
		break;

	case 9:										//1 MHz - 9.999 MHz
		//LL_TIM_SetCounter(TIMx, 0);
		LL_TIM_SetPrescaler(TIMx, 0);			//this is the same as for freq_range 8 but that
		LL_TIM_SetAutoReload(TIMx, 9);			//isn't important because the 1 MHz to 10 MHz
		LL_TIM_OC_SetCompareCH2(TIMx, 4);		//signal is taken from the Si5351 directly
		break;

	case 10:									//10 MHz - 40 MHz
		//LL_TIM_SetCounter(TIMx, 0);
		LL_TIM_SetPrescaler(TIMx, 0);			//this is the same as for freq_range 8 but that
		LL_TIM_SetAutoReload(TIMx, 9);			//isn't important because the 10 MHz to 40 MHz
		LL_TIM_OC_SetCompareCH2(TIMx, 4);		//signal is taken from the Si5351 directly
		break;

	default:									//should never happen
		LL_TIM_SetCounter(TIMx, 990);
		LL_TIM_SetPrescaler(TIMx, 0);
		LL_TIM_SetAutoReload(TIMx, 9999);
		LL_TIM_OC_SetCompareCH2(TIMx, 4999);

		break;
	}
}


void display_frequency(uint8_t channel, channel_settings ch)
{
	char buffer[20];
	uint32_t whole_part;
	uint32_t decimal_part;

	//channel 2 cannot produce higher than 1 MHz, because there is no room
	//for a selector left on the pcb, had to make messy fixes because of
	//these differences
	if (channel == CHANNEL2)
	{
		if (ch.freq_range > ch.max_freq_range) ch.freq_range = ch.max_freq_range;

		if (ch.frequency == ch.max_frequency)
		{
			ch.frequency /= 10;
			ch.freq_range = 9;
		}
	}



	switch (ch.freq_range)
	{
	case 1:
	case 4:
	case 7:
	case 10:
		whole_part = (uint32_t) (ch.frequency / 100000);
		decimal_part = (uint32_t) (ch.frequency - (whole_part * 100000));
		break;
	case 2:
	case 5:
	case 8:
		whole_part = (uint32_t) (ch.frequency / 10000);
		decimal_part = (uint32_t) (ch.frequency - (whole_part * 10000));
		break;
	case 0:
	case 3:
	case 6:
	case 9:
		whole_part = (uint32_t) (ch.frequency / 1000000);
		decimal_part = (uint32_t) (ch.frequency - (whole_part * 1000000));
		break;

	default://should not happen
		whole_part = (uint32_t) (ch.frequency / 10000);
		decimal_part = (uint32_t) (ch.frequency - (whole_part * 10000));
		break;
	}

	ST7735_WriteString(15, (30 + channel * 60), "               ", Font_11x18, ST7735_WHITE, ST7735_DARK_BLUE);
	if ((ch.freq_range == 1) || (ch.freq_range == 4) || (ch.freq_range == 7) || (ch.freq_range == 10)) sprintf(buffer, "%lu.%05lu", whole_part, decimal_part);
	if ((ch.freq_range == 2) || (ch.freq_range == 5) || (ch.freq_range == 8)) sprintf(buffer, "%lu.%04lu", whole_part, decimal_part);
	if ((ch.freq_range == 3) || (ch.freq_range == 6) || (ch.freq_range == 9) || (ch.freq_range == 0)) sprintf(buffer, "%lu.%06lu", whole_part, decimal_part);
	ST7735_WriteString(15, (30 + channel * 60), buffer, Font_11x18, ST7735_WHITE, ST7735_DARK_BLUE);

	if (ch.freq_range < 3) sprintf(buffer, "mHz");
	else if ((ch.freq_range >= 3) && (ch.freq_range < 6)) sprintf(buffer, "Hz");
	else if ((ch.freq_range >= 6) && (ch.freq_range < 9)) sprintf(buffer, "kHz");
	else sprintf(buffer, "MHz");
	ST7735_WriteString(120, (30 + channel * 60), buffer, Font_11x18, ST7735_WHITE, ST7735_DARK_BLUE);
}


void display_cursor(uint8_t channel, channel_settings ch)
{
	//here the ugly fix for channel 2 is also needed
	//when the frequency is 1 MHz

	if ((channel == CHANNEL2) && (ch.frequency == ch.max_frequency))
	{
		ch.freq_range = 9;
		ch.active_digit--;
	}


	//erase line
	ST7735_WriteString(15, (50 + channel * 60), "               ", Font_11x18, ST7735_WHITE, ST7735_DARK_BLUE);

	//place new cursor
	//digits 2 and 3 do not have a fixed position
	switch (ch.freq_range)
	{
	case 1:
	case 4:
	case 7:
	case 10:
		if (ch.active_digit == 6) ST7735_WriteString(26, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
		if (ch.active_digit == 5) ST7735_WriteString(48, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
		break;

	case 2:
	case 5:
	case 8:
		if (ch.active_digit == 6) ST7735_WriteString(26, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
		if (ch.active_digit == 5) ST7735_WriteString(37, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);

		break;
	case 0:
	case 3:
	case 6:
	case 9:
		if (ch.active_digit == 6) ST7735_WriteString(37, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
		if (ch.active_digit == 5) ST7735_WriteString(48, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);

		break;
	}

	//fixed position digits
	//digit 7
	if (ch.active_digit == 7) ST7735_WriteString(15, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);

	//digits 4, 3, 2 and 1
	if (ch.active_digit == 4) ST7735_WriteString(59, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
	if (ch.active_digit == 3) ST7735_WriteString(70, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
	if (ch.active_digit == 2) ST7735_WriteString(81, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
	if (ch.active_digit == 1) ST7735_WriteString(92, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);

	//mHz, Hz, kHz and MHz
	if (ch.active_digit == 0) ST7735_WriteString(120, (50 + channel * 60), "^", Font_11x18, ST7735_YELLOW, ST7735_DARK_BLUE);
}



void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
	while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_2);
	LL_RCC_HSE_Enable();
	while(LL_RCC_HSE_IsReady() != 1);
	LL_RCC_LSI_Enable();
	while(LL_RCC_LSI_IsReady() != 1);
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_9);
	LL_RCC_PLL_Enable();
	while(LL_RCC_PLL_IsReady() != 1);
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);
	LL_Init1msTick(72000000);
	LL_SetSystemCoreClock(72000000);
}


static void MX_I2C1_Init(void)
{
	LL_I2C_InitTypeDef I2C_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_6|LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_I2C1);

	LL_I2C_DisableOwnAddress2(I2C1);
	LL_I2C_DisableGeneralCall(I2C1);
	LL_I2C_EnableClockStretching(I2C1);

	I2C_InitStruct.PeripheralMode = LL_I2C_MODE_I2C;
	I2C_InitStruct.ClockSpeed = 100000;
	I2C_InitStruct.DutyCycle = LL_I2C_DUTYCYCLE_2;
	I2C_InitStruct.OwnAddress1 = 0;
	I2C_InitStruct.TypeAcknowledge = LL_I2C_ACK;
	I2C_InitStruct.OwnAddrSize = LL_I2C_OWNADDRESS1_7BIT;
	LL_I2C_Init(I2C1, &I2C_InitStruct);

	LL_I2C_SetOwnAddress2(I2C1, 0);
}


static void MX_IWDG_Init(void)
{
	//LL_IWDG_Enable(IWDG);
	//LL_IWDG_EnableWriteAccess(IWDG);
	//LL_IWDG_SetPrescaler(IWDG, LL_IWDG_PRESCALER_256);
	//LL_IWDG_SetReloadCounter(IWDG, 4095);
	//while (LL_IWDG_IsReady(IWDG) != 1);
	//LL_IWDG_ReloadCounter(IWDG);
}


static void MX_SPI2_Init(void)
{
	LL_SPI_InitTypeDef SPI_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_12|LL_GPIO_PIN_13|LL_GPIO_PIN_15;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_14;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	SPI_InitStruct.TransferDirection = LL_SPI_FULL_DUPLEX;
	SPI_InitStruct.Mode = LL_SPI_MODE_MASTER;
	SPI_InitStruct.DataWidth = LL_SPI_DATAWIDTH_8BIT;
	SPI_InitStruct.ClockPolarity = LL_SPI_POLARITY_LOW;
	SPI_InitStruct.ClockPhase = LL_SPI_PHASE_1EDGE;
	SPI_InitStruct.NSS = LL_SPI_NSS_HARD_OUTPUT;
	SPI_InitStruct.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV16;
	SPI_InitStruct.BitOrder = LL_SPI_MSB_FIRST;
	SPI_InitStruct.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
	SPI_InitStruct.CRCPoly = 10;
	LL_SPI_Init(SPI2, &SPI_InitStruct);
}


static void MX_TIM2_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 999;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM2, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM2);
	LL_TIM_OC_EnablePreload(TIM2, LL_TIM_CHANNEL_CH2);

	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 499;
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	LL_TIM_OC_Init(TIM2, LL_TIM_CHANNEL_CH2, &TIM_OC_InitStruct);

	LL_TIM_OC_DisableFast(TIM2, LL_TIM_CHANNEL_CH2);
	LL_TIM_SetTriggerInput(TIM2, LL_TIM_TS_TI1FP1);
	LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_EXT_MODE1);
	LL_TIM_IC_SetFilter(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
	LL_TIM_IC_SetPolarity(TIM2, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
	LL_TIM_DisableIT_TRIG(TIM2);
	LL_TIM_DisableDMAReq_TRIG(TIM2);
	LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM2);

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}


static void MX_TIM3_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 999;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM3, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM3);
	LL_TIM_OC_EnablePreload(TIM3, LL_TIM_CHANNEL_CH2);

	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM1;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 499;
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	LL_TIM_OC_Init(TIM3, LL_TIM_CHANNEL_CH2, &TIM_OC_InitStruct);

	LL_TIM_OC_DisableFast(TIM3, LL_TIM_CHANNEL_CH2);
	LL_TIM_SetTriggerInput(TIM3, LL_TIM_TS_TI1FP1);
	LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_EXT_MODE1);
	LL_TIM_IC_SetFilter(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV1);
	LL_TIM_IC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_RISING);
	LL_TIM_DisableIT_TRIG(TIM3);
	LL_TIM_DisableDMAReq_TRIG(TIM3);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM3);

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}


static void MX_TIM4_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);

	NVIC_SetPriority(TIM4_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM4_IRQn);

	TIM_InitStruct.Prescaler = 71;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 99;										//10 kHz (100 us) debouncing
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM4, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM4);
	LL_TIM_SetClockSource(TIM4, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM4, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM4);
}


static void MX_GPIO_Init(void)
{
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_13;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_10 | LL_GPIO_PIN_11;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_2 | LL_GPIO_PIN_3;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_8 | LL_GPIO_PIN_9 | LL_GPIO_PIN_11 | LL_GPIO_PIN_12;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_4 | LL_GPIO_PIN_5;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_10);
	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_11);
	LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_13);
}


void Error_Handler(void)
{

}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif
