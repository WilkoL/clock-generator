#ifndef __I2C1_H__
#define  __I2C1_H__

#include "main.h"

#define I2C_REQUEST_WRITE 	0x00
#define I2C_REQUEST_READ 	0x01



void I2C1_MEM8_Write(uint8_t dev_address, uint8_t dev_register, uint8_t* sendbuffer, uint16_t sendbuffer_length);
void I2C1_MEM8_Read(uint8_t dev_address, uint8_t dev_register, uint8_t* readbuffer, uint16_t readbuffer_length);

void I2C1_MEM16_Write(uint8_t dev_address, uint16_t dev_register, uint8_t* sendbuffer, uint16_t sendbuffer_length);
void I2C1_MEM16_Read(uint8_t dev_address, uint16_t dev_register, uint8_t* readbuffer, uint16_t readbuffer_length);

void I2C1_MEM8_Write_no_reg(uint8_t dev_address, uint8_t* sendbuffer, uint16_t sendbuffer_length);
void I2C1_MEM8_Read_no_reg(uint8_t dev_address, uint8_t* readbuffer, uint16_t readbuffer_length);


#endif
